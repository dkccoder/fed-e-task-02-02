const path = require('path')
const common = require('./webpack.common')
const { merge } = require('webpack-merge')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')


module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: 'js/[name]-[contenthash:8].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(jpg|png|gif|bmp|jpeg)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10 * 1024, // 10kb
            name: 'img/[name]-[contenthash:8].[ext]'
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'eslint-loader',
        enforce: 'pre'
      },
    ]
  }, 
  plugins: [
    // new MiniCssExtractPlugin({
    //   filename: 'css/[name]-[chunkhash:8].bundle.css'
    // }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: ['./public']
    })
  ]
})