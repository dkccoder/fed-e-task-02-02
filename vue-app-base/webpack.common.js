const path = require('path')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  mode: 'none',
  entry: './src/main.js',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
        },
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: /\.less$/,
        use: [
          "style-loader", 
          "css-loader", 
          "less-loader", 
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(jpg|png|gif|bmp|jpeg)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10 * 1024, // 10kb
          }
        }
      },
      {
          test: /\.html$/,
          use: {
            loader: 'html-loader',
            // options: {
            //   // 加载html中的资源文件
            //   attrs: ['img:src', 'a:href']
            // }
          }
        }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: 'webpack',
      filename:'index.html',
      template: './public/index.html',
      BASE_URL: './',
      options: {
        title: 'vue webpack'
      },
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name]-[chunkhash:8].bundle.css'
    }),
  ]
}