# 简答题
### 1.Webpack的构建流程中主要有哪些环节？如果可以请尽可能详尽描述Webpack打包的整个过程
**答**：
1. 配置出入口文件名和路径
2. 配置基础的loader来解析样式和脚本文件使页面正常工作
3. 配置额外的plugins优化开发体验，处理静态资源
4. 配置devServer开发服务器，使开发阶段的代码能在浏览器运行
5. 将webpack配置内容根据不同的环境导出不同的配置


### 2.Loader和Plugin有哪些不同？请描述一下开发Loader和Plugin的思路
**答**：Loader是webpack用来处理不同的文件，将他们转换为能够适应浏览器和项目运行的文件。Plugin是解决loader无法实现的事，比如压缩和打包优化，还可以动态生成html文件，清空之前生成的打包文件等。开发loader只需要定义一个函数，参数为上一个loader产生的结果或资源文件，根据指定的test正则来匹配相应的文件后缀，然后使用对应的loader插件，将处理过的文件输出到dist相应的目录中。开发plugin是可以是类，也可以是函数，参数为包含所有配置信息的compiler，并通过它来注册勾子函数，获取此次打包的上下文和配置信息，在拿到的文件内容中进行处理并导出。


# 编程题说明
**答**：先配置公共的common.js文件，公共部分有基础的输入输出，然后配置loader，其中用到的loader有js-loader处理脚本文件，css-loader和style-loader处理css文件，项目中使用到了less预处理器，所以还需要less-loader结合css和style处理less文件，另外还有html-loader处理模板文件，还有vue-loader处理vue文件，vue-loader还需要vue-template-compiler插件来配合使用，才能将vue中的html、js、css分离开来，最后还有图片处理的url-loader。plugins插件中用到了VueLoaderPlugin，这是vue-loader必需的依赖；还用到了HtmlWebpackPlugin插件，根据public/html生成dist下的入口文件，最后使用了MiniCssExtractPlugin插件将css文件提取为独立的文件，也就可以将之前用到的style-loader替换为这个插件自带的loader。然后配置dev.js文件，将mode设为development，并且启用了source map可以定位到源文件的报错，还设置了开发服务器devServer，并在plugins中添加了热更新插件，引入common.js文件使用webpack-merge与现有配置合并。最后配置prod.js文件，这里需要将mode设为production，给之前common.js中生成到dist目录的文件设置更为合理的目录结构，并添加chunk名，然后还要使用CleanWebpackPlugin和CopyWebpackPlugin插件清除掉之前的dist目录和复制静态文件，在上线之前还需要进行eslint检验，所以需要运行eslint-loader。